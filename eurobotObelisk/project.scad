// Set parameters
length = 98;
width = 98;
height = 250;
wall_thickness = 2;
bottom_thickness = 3;
hole_diameter = 9.9;
// Calculate inner dimensions
inner_length = length - 2 * wall_thickness;

difference()
{
    cube([ length, width, height ]);
    translate([ wall_thickness, wall_thickness, bottom_thickness ])
        cube([ length - 2 * wall_thickness, width, height ]);
    translate([ length / 2, width / 2, -1 ]) cylinder(d = hole_diameter, h = bottom_thickness + 2);
}
