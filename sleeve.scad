module sleeve()
{
    difference()
    {
        cylinder(h = 11, d = 9.4, center = true, $fn = 100);
        translate([ 0, 0, 0 ]) cylinder(h = 13, d = 5, center = true, $fn = 100);
    }
}
fac = 21;
render()
{
    for (i = [0:4])
    {
        for (j = [0:5])
        {
            translate([ i * fac, j * fac, 0 ]) import("./stl/Winkel.stl");
        }
    }
}
