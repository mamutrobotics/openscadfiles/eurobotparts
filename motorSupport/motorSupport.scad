module motorSupport(depth, hight, leng, thick)
{

    difference()
    {
        union()
        {
            cube([ leng, thick, hight ]);
            cube([ leng, depth, thick ]);
            for (i = [ 0, 14.5, 0 ])
            {
                translate([ i, 0, 0 ]) cube([ thick, depth, hight ]);
            }
        }
        translate([ -1, depth + thick + 5, hight ]) rotate([ 0, 90, 0 ]) cylinder(r = 39, h = leng + 2, $fn = 500);
        for (i = [ 7, 27 ])
        {
            hull()
            {
                for (j = [ 8, depth ])
                    translate([ i, j, -1 ]) cylinder(h = 5, d = 5.2, $fn = 500);
            }
        }
        for (i = [ 15.1, 39 ])
        {
            for (j = [ 6, 20 ])
            {
                hull()
                {
                    for (k = [ 0, 6 ])
                    {
                        translate([ j + k, 4, i ]) rotate([ 90, 0, 0 ]) cylinder(h = thick + 2, d = 2.7, $fn = 500);
                    }
                }
            }
        }
    }
}
module motorSupportCuttet()
{
    leng = 32;
    thick = 3;
    hight = 44;
    depth = 35;
    difference()
    {
        motorSupport(depth, hight, leng, thick);
        translate([ -1, depth + thick, -1 ]) rotate([ 0, 0, -30 ]) cube([ hight + 5, leng, hight + 2 ]);
    }
}
// render()
//{
//  mirror([ 1, 0, 0 ]) motorSupportCuttet();
//  translate([ 0.5, 0, 0 ]) motorSupport(35, 44, 32, 3);
//  translate([ 33, 0, 0 ]) motorSupport(35, 44, 32, 3);
mirror([ 1, 0, 0 ]) motorSupport(35, 44, 32, 3);
translate([ 0.5, 0, 0 ]) motorSupportCuttet();
//}
