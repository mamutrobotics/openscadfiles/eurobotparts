difference()
{
    hull()
    {
        translate([ 15, 15, 0 ]) cylinder(r = 15, h = 30, $fn = 500);
        translate([ 285, 15, 0 ]) cylinder(r = 15, h = 30, $fn = 500);
    }
    for (i = [15:30:300])
    {
        translate([ i, 15, 12 ]) cylinder(r = 6, h = 19);
    };
};
