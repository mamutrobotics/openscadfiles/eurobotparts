latexmk -lualatex --shell-escape -pdflua -dvi readMe.tex
htlatex readMe.tex
latexmk -c readMe.tex
mv ./readMe.pdf ../
rm *.dvi *.idv *.lg *.xref *.tmp *.4tc
